from KleyMomentConst import KleyMomentFilePathSignature
import sys

if len(sys.argv) != 2:
    print("Usage: python3 KleyMoment.py inputKleyMomented.js")
    exit(1)

filename = sys.argv[1]
fileDescriptor = open(filename, 'r')
currentFilePath = ""
currentFileDescriptor = None
for line in fileDescriptor:
    if line.startswith(KleyMomentFilePathSignature):
        if (currentFileDescriptor != None):
            currentFileDescriptor.close()
        currentFilePath = line[len(KleyMomentFilePathSignature):].rstrip()
        currentFileDescriptor = open(currentFilePath, mode = 'w')
    elif currentFileDescriptor != None:
        currentFileDescriptor.write(line)

fileDescriptor.close()
currentFileDescriptor.close()
