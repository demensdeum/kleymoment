from KleyMomentConst import KleyMomentFilePathSignature
import os
import sys

if len(sys.argv) != 4:
    print("Usage: python3 KleyMoment.py js someDirectory outputFile.js")
    exit(1)

fileExtension = sys.argv[1]
directory = sys.argv[2]
outputFile = sys.argv[3]

print(f"fileExtension: {fileExtension}")
print(f"directory: {directory}")
print(f"outputFile: {outputFile}")

content = ""

for root, dirs, files in os.walk(directory):
    for file in files:
        if file.endswith(f".{fileExtension}"):
             filepath = os.path.join(root, file)
             content += f"{KleyMomentFilePathSignature}{filepath}"
             content += "\n"
             fileDescriptor = open(filepath, mode = 'r')
             fileContent = fileDescriptor.read()
             fileDescriptor.close()
             content += fileContent

outputFileDescriptor = open(outputFile, mode = 'w')
outputFileDescriptor.write(content)
outputFileDescriptor.close()
